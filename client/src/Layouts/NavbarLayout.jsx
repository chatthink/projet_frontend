import React, { useState } from 'react';
import { NavLink } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import io from 'socket.io-client';
import { useTranslation } from 'react-i18next';
import { getData } from '../Actions/UserActions';
import * as S from '../Styles/Components/jsx/navbar';
import Logo from '../Assets/images/logo.png';
import FlagFR from '../Assets/images/flag--fr.png';
import FlagEN from '../Assets/images/flag--en.png';
import { API } from '../Utils/config';

const socket = io(API);

function NavbarLayout() {
  const [t, i18n] = useTranslation('global');
  const dispatch = useDispatch();
  const isLoggedIn = useSelector((state) => state.userReducer.user);
  const [showLinks, setShowLinks] = useState(false);
  const handleShowLinks = () => {
    setShowLinks(!showLinks);
  };
  const sendLogout = async () => {
    await dispatch(getData('logout', 'logout_user'));
    socket.emit('disconnectUser', isLoggedIn);
    window.location = '/';
  };

  return (
    <S.Nav>
      <div className="header__logo">
        <NavLink to="/chats">
          <img src={Logo} alt="logo" width="50" height="50" />
        </NavLink>
      </div>
      <S.NavSection>
        {isLoggedIn ? (
          <div>
            <div className="header">
              <div className="header__translation">
                <button type="button" onClick={() => i18n.changeLanguage('fr')}>
                  <img
                    src={FlagFR}
                    alt="Flag FR"
                  />
                </button>
                <button type="button" onClick={() => i18n.changeLanguage('en')}>
                  <img
                    src={FlagEN}
                    alt="Flag EN"
                  />
                </button>
              </div>
              <NavLink
                to="/chats"
                className={({ isActive }) => (isActive ? 'active' : 'header__navbar__links text-uppercase')}
              >
                {t('navbarContent.messages')}
              </NavLink>
              <NavLink
                to="/users"
                className={({ isActive }) => (isActive ? 'active' : 'header__navbar__links text-uppercase')}
              >
                {t('navbarContent.users')}
              </NavLink>
              <NavLink
                to="/logout"
                onClick={() => sendLogout()}
                className={({ isActive }) => (isActive ? 'active' : 'header__navbar__links text-uppercase')}
              >
                {t('navbarContent.logout')}
              </NavLink>
            </div>

            <nav
              className={`${showLinks ? 'mobile-navbar__show-nav' : 'hide-nav'
              } mobile-navbar`}
            >
              <button
                type="button"
                className="mobile-navbar__burger"
                onClick={handleShowLinks}
              >
                <span className="mobile-navbar__burger-bar" />
              </button>
              <div
                className={`${showLinks ? 'mobile-navbar--show' : 'mobile-navbar--hide'
                }`}
              >
                <div className="mobile-navbar__wrapper">
                  <NavLink
                    to="/chats"
                    onClick={handleShowLinks}
                    className={({ isActive }) => (isActive
                      ? 'active'
                      : 'mobile-navbar__wrapper__links text-uppercase')}
                  >
                    {t('navbarContent.messages')}
                  </NavLink>
                  <NavLink
                    to="/users"
                    onClick={handleShowLinks}
                    className={({ isActive }) => (isActive
                      ? 'active'
                      : 'mobile-navbar__wrapper__links text-uppercase')}
                  >
                    {t('navbarContent.users')}
                  </NavLink>
                  <NavLink
                    to="/logout"
                    onClick={sendLogout}
                    className={({ isActive }) => (isActive
                      ? 'active'
                      : 'mobile-navbar__wrapper__links text-uppercase')}
                  >
                    {t('navbarContent.logout')}
                  </NavLink>
                  <div className="header__translation">
                    <button type="button" onClick={() => i18n.changeLanguage('fr')}>
                      <img
                        src={FlagFR}
                        alt="Flag FR"
                      />
                    </button>
                    <button type="button" onClick={() => i18n.changeLanguage('en')}>
                      <img
                        src={FlagEN}
                        alt="Flag EN"
                      />
                    </button>
                  </div>
                </div>
              </div>
            </nav>
          </div>
        ) : (
          <div>
            <div className="header">
              <div className="header__translation">
                <button type="button" onClick={() => i18n.changeLanguage('fr')}>
                  <img
                    src={FlagFR}
                    alt="Flag FR"
                  />
                </button>
                <button type="button" onClick={() => i18n.changeLanguage('en')}>
                  <img
                    src={FlagEN}
                    alt="Flag EN"
                  />
                </button>
              </div>

              <NavLink
                to="/register"
                className={({ isActive }) => (isActive ? 'active' : 'header__navbar__links text-uppercase')}
              >
                {t('headerContent.signup')}
              </NavLink>
              <NavLink
                to="/login"
                className={({ isActive }) => (isActive ? 'active' : 'header__navbar__links text-uppercase')}
              >
                {t('headerContent.login')}
              </NavLink>
            </div>
            <nav
              className={`${showLinks ? 'mobile-navbar__show-nav' : 'hide-nav'
              } mobile-navbar`}
            >
              <button
                type="button"
                className="mobile-navbar__burger"
                onClick={handleShowLinks}
              >
                <span className="mobile-navbar__burger-bar" />
              </button>
              <div
                className={`${showLinks ? 'mobile-navbar--show' : 'mobile-navbar--hide'
                }`}
              >
                <div className="mobile-navbar__wrapper">
                  <NavLink
                    to="/register"
                    onClick={handleShowLinks}
                    className={({ isActive }) => (isActive ? 'active' : 'mobile-navbar__wrapper__links text-uppercase')}
                  >
                    {t('headerContent.signup')}
                  </NavLink>
                  <NavLink
                    to="/login"
                    onClick={handleShowLinks}
                    className={({ isActive }) => (isActive ? 'active' : 'mobile-navbar__wrapper__links text-uppercase')}
                  >
                    {t('headerContent.login')}
                  </NavLink>
                  <div className="header__translation">
                    <button type="button" onClick={() => i18n.changeLanguage('fr')}>
                      <img
                        src={FlagFR}
                        alt="Flag FR"
                      />
                    </button>
                    <button type="button" onClick={() => i18n.changeLanguage('en')}>
                      <img
                        src={FlagEN}
                        alt="Flag EN"
                      />
                    </button>
                  </div>
                </div>
              </div>
            </nav>
          </div>
        )}
      </S.NavSection>
    </S.Nav>
  );
}

export default NavbarLayout;
