import { CRYPTE_KEY } from './config';

const aes256 = require('aes256');

const key = CRYPTE_KEY;

export const DoEncrypt = (text) => {
  const encrypted = aes256.encrypt(key, text);
  return encrypted;
};

export const DoDecrypt = (cipher) => {
  const decrypted = aes256.decrypt(key, cipher);
  return decrypted;
};
