import React from 'react';
import { Spinner } from 'react-bootstrap';

function SpinnerComponent() {
  return (
    <div className="justify-content-center d-flex">
      <Spinner animation="border" role="status" className="colors--1" />
    </div>

  );
}

export default SpinnerComponent;
