import React, {
  useState, useEffect, useRef, memo,
} from 'react';
import { useSelector, useDispatch } from 'react-redux';
import axios from 'axios';
import io from 'socket.io-client';
import PropTypes from 'prop-types';
import { MdSend, MdAttachFile } from 'react-icons/md';
import { useTranslation } from 'react-i18next';
import { DoEncrypt } from '../Utils/aes';
import { API, API_URL } from '../Utils/config';
import UsersListChatComponent from './UsersListChatComponent';
import setMessageIsRead from '../Actions/ChatActions';
import MessageComponent from './MessageComponent';
import * as C from '../Styles/Components/jsx/chat-box';
import * as I from '../Styles/Components/jsx/input';
import SpinnerComponent from './SpinnerComponent';

const socket = io(API);
function ChatBoxComponent({ match }) {
  // Use the 'useTranslation' hook from the 'react-i18next'
  // library to get the 't' function for translations
  const [t] = useTranslation('global');

  // Destructure the 'id' property from the 'match' object in the URL params
  const friendId = match?.params.id;

  // Use the 'useState' hook to create a state variable called
  // 'state' with an initial value of an empty string
  const [state, setstate] = useState('');

  // Use the 'useDispatch' hook from the 'react-redux' library to get the 'dispatch' function
  const dispatch = useDispatch();

  // Use the 'useSelector' hook from the 'react-redux' library to get the 'user'
  // property from the 'userReducer' slice of the Redux store
  const userId = useSelector((state) => state.userReducer.user);

  // Use the 'useSelector' hook from the 'react-redux' library to get
  // the 'username' property from the 'userReducer' slice of the Redux store
  const username = useSelector((state) => state.userReducer.username);

  // Use the 'useState' hook to create a state variable
  // called 'onlineUsers' with an initial value of an empty array
  const [onlineUsers, setOnlineUsers] = useState([]);

  // Use the 'useState' hook to create a state variable called
  // 'newestSocketPrivateMessage' with an initial value of 'null'
  const [newestSocketPrivateMessage, setNewestSocketPrivateMessage] = useState(null);

  // Use the 'useState' hook to create a state variable called 'foundUser'
  // with an initial value of an empty object
  const [foundUser, setFoundUser] = useState({});

  // Use the 'useState' hook to create a state variable called
  // 'foundMessages' with an initial value of an empty array
  const [foundMessages, setFoundMessages] = useState([]);

  // Use the 'useState' hook to create a state variable called
  // 'msg' with an initial value of an empty string
  const [msg, setMessage] = useState('');

  // Use the 'useState' hook to create a state variable called
  // 'isLoading' with an initial value of 'false'
  const [isLoading, setIsLoading] = useState(false);

  // Use the 'useState' hook to create a state variable
  // called 'warning' with an initial value of 'undefined'
  const [warning, setWarning] = useState(undefined);

  // Use the 'useRef' hook to create a mutable ref object with an initial value of 'undefined'
  const readMsg = useRef();

  useEffect(() => {
    // Attach an event listener for the 'privatemessage' event on the 'socket' object
    socket.on('privatemessage', (pm) => {
      // Check if the author of the private message is the current user or the user being contacted
      if (pm.authorId === foundUser._id || pm.authorId === userId) {
        // If the condition is true, set the private message as the newest
        // private message received through the socket
        setNewestSocketPrivateMessage(pm);
      } else {
        // If the condition is false, dispatch an action to set the message as unread
        dispatch(
          setMessageIsRead({
            read: false,
            authorId: pm.authorId,
          }),
        );
      }
    });
    // Return a cleanup function to remove the event listener when the component unmounts
    return () => socket.removeAllListeners('privatemessage');
  }, [foundUser, userId, dispatch]);

  useEffect(() => {
    // Check if the 'newestSocketPrivateMessage' state variable has a truthy value
    if (newestSocketPrivateMessage) {
      // If the condition is true, update the 'foundMessages' state variable by adding
      // the 'newestSocketPrivateMessage' to the beginning of the array
      setFoundMessages((existingMessages) => [
        ...existingMessages,
        newestSocketPrivateMessage,
      ]);
    }
  }, [newestSocketPrivateMessage]);

  useEffect(() => {
    // Emit an 'adduser' event on the 'socket' object with the 'userId' and 'username' as arguments
    socket.emit('adduser', userId, username);
    // Attach an event listener for the 'getusers' event on the 'socket' object
    socket.on('getusers', (users) => {
      // Set the 'onlineUsers' state variable to the array
      // of online users received from the 'getusers' event
      setOnlineUsers([users]);
    });
    // Return a cleanup function to remove the event listener when the component unmounts
    return () => socket.removeAllListeners('getusers');
  }, [userId, username]);

  useEffect(() => {
    // Define an async function called 'getData'
    const getData = async () => {
      // Check if the 'friendId' variable has a truthy value
      if (friendId) {
        // If the condition is true, make an HTTP GET request
        // to the '/user/:id' endpoint with the 'friendId' as the URL param
        const returnedUser = await axios.get(`${API_URL}/user/${friendId}`);
        // If the request is successful, set the 'foundUser'
        // state variable to the data received from the API
        if (returnedUser) {
          setFoundUser(returnedUser.data);
        }
        // Make an HTTP GET request to the '/privatemessages'
        // endpoint with the 'userId' and 'friendId' as query params
        const foundMsg = await axios.get(
          `${API_URL}/privatemessages?userid=${userId}&friendid=${friendId}`,
        );
        // If the request is successful and there are messages found,
        // set the 'foundMessages' state variable to the array of messages received from the API
        if (foundMsg.data.length > 0) {
          setFoundMessages(foundMsg.data);
        }
      }
    };
      // Execute the 'getData' function
    getData();
  }, [userId, friendId]);

  // Define a function called 'readInput' that takes an event object as an argument
  const readInput = (e) => {
    // Set the 'msg' state variable to the value of the input field
    setMessage(e.target.value);
  };

  // Define a function called 'handleFileUpload' that takes an event object as an argument
  const handleFileUpload = (e) => {
    // Set the 'value' property of the 'readMsg' ref object to the file name of the selected file
    readMsg.current.value = e.target.files[0].name;
    // Set the 'msg' state variable to the file object of the selected file
    setMessage(e.target.files[0]);
  };

  // Define an async function called 'submitHandler' that takes an event object as an argument
  const submitHandler = async (e) => {
    // Prevent the default action of the event
    e.preventDefault();
    // Check if the 'msg' state variable has a truthy value and is a string
    if (msg.length > 0) {
      try {
        // If the condition is true, encrypt the message using the 'DoEncrypt' function
        const encryptMsg = DoEncrypt(msg);
        // Create an object called 'message' with the following properties
        const message = {
          author: username,
          authorId: userId,
          content: encryptMsg,
          receiver: foundUser._id,
          receiverName: foundUser.username,
          participants: [userId, foundUser._id],
          type: 'textMessage',
        };
        // Set the 'isLoading' state variable to true
        setIsLoading(true);
        // Set the 'warning' state variable to the translated string for the
        // 'chatContent.warning' key
        setWarning(t('chatContent.warning'));
        // Make an HTTP POST request to the '/messages'
        // endpoint with the 'message' object as the request body
        const res = await axios.post(`${API_URL}/messages`, message);
        // Update the 'foundMessages' state variable by
        // adding the response data to the beginning of the array
        setFoundMessages((existingMessages) => [...existingMessages, res.data]);
        // If the response data is not an empty array,
        // emit a 'privatemsg' event on the 'socket' object
        // with the response data as an argument, and set the
        // 'isLoading' and 'warning' state variables to their default values
        if ([res.data].length > 0) {
          socket.emit('privatemsg', message);
          setIsLoading(false);
          setWarning(undefined);
        }
        // Set the 'msg' state variable to an empty string
        setMessage('');
        // Set the 'value' property of the 'readMsg' ref object to an empty string
        readMsg.current.value = '';
      } catch (err) {
        // If an error occurs, log the error to the console
        // console.log(err);
      }
    } else if (msg.name) {
      // If the 'msg' state variable is not a string and has a truthy 'name' property,
      try {
        // Create a new instance of the FormData object
        const formData = new FormData();
        // Append the following properties to the 'formData' object
        formData.append('author', username);
        formData.append('authorId', userId);
        formData.append('content', msg);
        formData.append('receiver', foundUser._id);
        formData.append('receiverName', foundUser.username);
        formData.append('participants', [userId, foundUser._id]);
        formData.append('type', '');
        // Set the 'isLoading' state variable to true
        setIsLoading(true);
        // Set the 'warning' state variable to the translated
        // string for the 'chatContent.warning' key
        setWarning(t('chatContent.warning'));
        // sends a POST request to the specified URL with the specified form data.
        // The await keyword is used to wait for the response
        // from the server. The response is stored in the res constant
        const res = await axios.post(`${API_URL}/messages`, formData);
        // Updates the state of the foundMessages variable by
        // adding the data from the response to the existing messages
        setFoundMessages((existingMessages) => [...existingMessages, res.data]);
        // Checks if the data from the response has a length
        // greater than 0. If it does, the code inside the curly braces will be executed
        if ([res.data].length > 0) {
          // Sends a message over a socket connection with
          // the event type 'privatemsg' and the data from the response
          socket.emit('privatemsg', res.data);
          // Updates the state of the isLoading variable to false
          setIsLoading(false);
          // Updates the state of the warning variable to undefined
          setWarning(undefined);
        }
        // Updates the state of the message variable to an empty string
        setMessage('');
        // Sets the value of the element referenced by the readMsg ref to an empty string
        readMsg.current.value = '';
      } catch (err) {
        // console.log(err);
      }
    }
  };

  if (userId) {
    return (
      <div className="chat-box__container">
        <C.ChatContent>
          <UsersListChatComponent
            onlineUsers={onlineUsers}
            foundUser={foundUser}
          />
          <C.ChatRightSection>
            {warning && (
            <div className="auth__error">
              <p>{warning}</p>
            </div>
            )}
            {isLoading ? <SpinnerComponent /> : ''}
            <MessageComponent
              foundMessages={foundMessages}
              userId={userId}
              friendId={friendId}
              foundUser={foundUser}
            />
            <form onSubmit={submitHandler} encType="multipart/form-data">
              <div className="chat-box__container__wrapper">
                <div className="chat-box__container__input">
                  <I.InputMessage
                    ref={readMsg}
                    disabled={!friendId}
                    autoComplete="off"
                    onChange={readInput}
                    name="inputMsg"
                    type="text"
                    placeholder={t('chatContent.enter_message')}
                  />
                  <button name="inputMsg" type="submit">
                    <MdSend size={25} />
                  </button>
                </div>
                <label htmlFor="upload">
                  <input
                    onChange={handleFileUpload}
                    type="file"
                    id="upload"
                    hidden
                  />
                  <div className="chat-box__container__wrapper__upload">
                    <MdAttachFile id="Uploadbutton" size={25} color="white" />
                  </div>
                </label>
              </div>
            </form>
          </C.ChatRightSection>
        </C.ChatContent>
      </div>
    );
  }
  setTimeout(() => {
    setstate(t('chatContent.time_exceeded'));
  }, 500);
  return (
    <div className="chat-box__container">
      <h2>{state}</h2>
      {' '}
    </div>
  );
}

export default memo(ChatBoxComponent);

ChatBoxComponent.propTypes = {
  match: PropTypes.oneOfType([PropTypes.object]),
};

ChatBoxComponent.defaultProps = {
  match: null,
};
