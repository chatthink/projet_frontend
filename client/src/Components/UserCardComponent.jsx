import React from 'react';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import * as S from '../Styles/Components/jsx/card';
import * as B from '../Styles/Components/jsx/button';

function UserCardComponent({ item }) {
  const [t] = useTranslation('global');

  return (
    <S.CardItem key={item.id}>
      <div className="user-card__picture">
        <img
          alt="User"
          src="https://images.unsplash.com/photo-1671538856783-c0b123a2223f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80"
        />
      </div>
      <div className="user-card__text">
        <div className="user-card__infos">
          <h3>
            {t('cardContent.pseudo')}
            {' '}
            :
            {' '}
            {item.username}
          </h3>
          <p>
            {t('cardContent.mail')}
            :
            {' '}
            {item.email}
          </p>
          <S.CardDate>
            {t('cardContent.date')}
            :
            {' '}
            {item.date}
          </S.CardDate>
        </div>
        <Link to={`/chats/${item._id}`}>
          <B.Button>
            {t('cardContent.send_message')}
            {' '}
            {item.username}
          </B.Button>
        </Link>

      </div>
    </S.CardItem>
  );
}

export default UserCardComponent;

UserCardComponent.propTypes = {
  item: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
};
