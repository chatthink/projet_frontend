import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { ImBubbles2 } from 'react-icons/im';
import { HiOutlineStatusOnline } from 'react-icons/hi';
import { useTranslation } from 'react-i18next';
import { getData } from '../Actions/UserActions';
import * as C from '../Styles/Components/jsx/chat-box';
import * as B from '../Styles/Components/jsx/button';

// Define a functional component called 'UsersListChatComponent'
// that takes an object with the 'foundUser' and 'onlineUsers' properties as an argument
function UsersListChatComponent({ foundUser, onlineUsers }) {
  // Use the 'useSelector' hook from the 'react-redux'
  // library to get the 'user' property from the global state object
  const userID = useSelector((state) => state.userReducer.user);

  // Use the 'useDispatch' hook from the 'react-redux'
  // library to get the 'dispatch' function for dispatching actions
  const dispatch = useDispatch();

  // Use the 'useState' hook to create a state
  // variable called 'users' with an empty array as its initial value
  const [users, setUsers] = useState([]);
  const [onUsers, setOnUsers] = useState([]);

  // Use the 'useState' hook to create a state variable
  // called 'conversations' with an empty array as its initial value
  const [conversations, setConversations] = useState([]);

  // Use the 'useSelector' hook from the 'react-redux' library
  // to get the 'username' property from the global state object
  const username = useSelector((state) => state.userReducer.username);

  // Use the 'useTranslation' hook from the 'react-i18next'
  // library to get the 't' function for translating strings
  const [t] = useTranslation('global');

  useEffect(() => {
    if (onlineUsers.length > 0) {
      for (let i = 0; i < onlineUsers[0].length; i += 1) {
        if (onlineUsers[0][i].userID !== null) {
          setOnUsers(onlineUsers[0].length - 1);
        }
      }
    } else {
      setOnUsers('0');
    }
  }, [onlineUsers]);

  // Use the 'useEffect' hook to get all messages and
  // create a list of unique users that the current user has chatted with
  useEffect(() => {
    async function getMessages() {
      // Dispatch an action using the 'getData' action creator and the 'dispatch' function
      const response = await dispatch(
        getData('getallmessages', 'get_all_messages'),
      );
      // Get the 'messages' property from the response object
      const { messages } = response.payload.data;
      // Create a new Set object to store unique user IDs
      const unique = new Set();
      // Iterate over the messages array
      messages.forEach((item) => {
        // If the current user is the receiver of the message,
        // add the message sender's ID and username to the Set
        if (item.receiver === userID) {
          unique.add(`${item.author}|${item.authorId}`);
        }
        // If the current user is the sender of the message,
        // add the message receiver's ID and username to the Set
        if (item.authorId === userID) {
          unique.add(`${item.receiverName}|${item.receiver}`);
        }
      });
      // Update the 'users' state variable with an array
      // of user ID and username pairs that have been split on the '|' character
      setUsers([...unique].map((item) => item.split('|')));
    }
    getMessages();
  }, [dispatch, userID]);

  // Use the 'useEffect' hook to create a list
  // of conversations that the current user has had with other users
  useEffect(() => {
    // If the 'users' array is not empty
    if (users.length > 0) {
      // Initialize an empty array called 'data'
      const data = [];
      // Iterate over the 'users' array
      for (let i = 0; i < users.length; i += 1) {
        // Get the current user pair
        const value = users[i];
        // Iterate over the current user pair
        for (let j = 0; j < value.length - 1; j += 1) {
          // If the current value is not false or undefined,
          // add an object to the 'data' array with the user's ID and username
          if (value !== false && value !== undefined) {
            data.push({ userId: value[j + 1], username: value[j] });
          }
        }
      }
      // If the 'data' array is not empty
      if (data.length > 0) {
        // Check if the current conversation's user is already in the 'data' array
        const bool = data.some((item) => item.userId === foundUser._id);
        // If the current conversation's user is not in
        // the 'data' array, add them to the 'conversations' state variable
        if (!bool) {
          // If the current conversation's user is not in the 'data' array,
          // update the 'conversations' state variable with the current 'data' array
          setConversations(data);
          // Add the current conversation's user to the 'conversations' state variable
          setConversations((prev) => [
            ...prev,
            {
              userId: foundUser._id,
              username: foundUser.username,
            },
          ]);
        } else {
          // If the current conversation's user is already in the 'data' array,
          // update the 'conversations' state variable with the current 'data' array
          setConversations(data);
        }
      }
    }
  }, [foundUser, users, userID]);

  return (
    <C.ChatLeftSection>
      <h3 className="chat-box__users-list__online">
        {t('chatContent.online')}
        {' '}
        (
        {onUsers}
        )
        {' '}
        <HiOutlineStatusOnline className="colors--2" />
      </h3>
      <div className="chat-box__users-list__online__wrapper">
        {onlineUsers.length > 0 ? onlineUsers[0].map(
          (item) => (item.userId && item.userId !== userID ? (
            <Link className="text-decoration-none" key={item.userId} to={`/chats/${item.userId} `}>
              <div
                className={item.userId === foundUser._id ? 'chat-box__users-list__users chat-box__users-list__users--active ' : 'chat-box__users-list__users'}
              >
                <span>
                  <h3>{item.username}</h3>
                </span>
              </div>
            </Link>
          ) : (
            item.userId && (
            <Link className="text-decoration-none" key={item.userId} to="/chats">
              <div className={window.location.pathname === '/chats' ? 'chat-box__users-list__users chat-box__users-list__users--active ' : 'chat-box__users-list__users'}>
                <span>
                  <h3>
                    <img
                      alt="User"
                      src="https://images.unsplash.com/photo-1671538856783-c0b123a2223f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80"
                    />
                    {username}
                    {' '}
                    (
                    {t('chatContent.me')}
                    )
                  </h3>
                </span>
              </div>
            </Link>
            )
          )),
        ) : ''}
      </div>
      <C.ChatDiv>
        <h3 className="chat-box__users-list__conversations">
          {t('navbarContent.messages')}
          {' '}
          <ImBubbles2 />
        </h3>
        <div className="chat-box__users-list__overflow">
          {conversations.length > 0 ? (conversations.map((item) => item.userId !== undefined && (
            <Link className="text-decoration-none" key={item.userId} to={`/chats/${item.userId} `}>
              <div
                aria-hidden="true"
                className={item.userId === foundUser._id
                  ? 'chat-box__users-list__users chat-box__users-list__users--active' : 'chat-box__users-list__users'}
              >
                <div className="d-flex">
                  <img
                    alt="User"
                    src="https://images.unsplash.com/photo-1671538856783-c0b123a2223f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80"
                  />
                  <h3>{item.username}</h3>
                </div>
              </div>
            </Link>
          ))
          ) : (
            <div className="chat-box__users-list__void">
              <p>
                {t('chatContent.no_message')}
                {' '}
                <br />
                {' '}
                <Link to="/users">
                  <B.Button>
                    {t('chatContent.add_user')}
                    {' '}
                  </B.Button>
                </Link>
              </p>
            </div>
          )}
        </div>
      </C.ChatDiv>
    </C.ChatLeftSection>
  );
}

export default UsersListChatComponent;
UsersListChatComponent.propTypes = {
  foundUser: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
    .isRequired,
  onlineUsers: PropTypes.oneOfType([PropTypes.object, PropTypes.array])
    .isRequired,
};
