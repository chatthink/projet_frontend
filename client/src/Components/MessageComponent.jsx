/* eslint-disable no-nested-ternary */
import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import * as timeago from 'timeago.js';
import TimeAgo from 'timeago-react';
import fr from 'timeago.js/lib/lang/fr';
import en from 'timeago.js/lib/lang/en_US';
import { useTranslation } from 'react-i18next';
import { DoDecrypt } from '../Utils/aes';
import * as C from '../Styles/Components/jsx/chat-box';
import Logo from '../Assets/images/logo.png';

function MessageComponent({
  foundMessages, userId,
}) {
  // Use the 'useTranslation' hook from the 'react-i18next'
  // library to get the 't' function for translating strings
  const [t] = useTranslation('global');

  // Register the 'fr' and 'en' languages with the 'timeago' library
  timeago.register('fr', fr);
  timeago.register('en', en);

  // Use the 'useRef' hook to create a ref object called 'scrollRef'
  const scrollRef = useRef();

  // Use the 'useEffect' hook to scroll to the bottom of
  // the chat window when the 'foundMessages' state variable changes
  useEffect(() => {
    scrollRef.current?.scrollIntoView({ behavior: 'smooth' });
  }, [foundMessages]);

  // Define a function called 'decryptToPlainText' that
  // takes a cipher text as an argument and returns the decrypted plain text
  const decryptToPlainText = (cipher) => DoDecrypt(cipher);

  return (
    <C.ChatRightContent>
      <div className="chat-box__messages">
        {
      foundMessages.length !== 0 ? foundMessages.map((data) => (
        <div ref={scrollRef} key={data._id} className={data.authorId === userId ? 'chat-box__messages__right' : 'chat-box__messages__left'}>
          <h6>
            {' '}
            <img
              alt="User"
              src="https://images.unsplash.com/photo-1671538856783-c0b123a2223f?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80"
            />
            {' '}
            {data.author}
          </h6>
          <p>
            {data.type === 'textMessage' ? decryptToPlainText(data.content)
              : data.type === 'image/jpeg' || data.type === 'image/png'
                ? <img src={data.content} alt="Pic" className="chat-box__messages__images" />
                : (
                  <video className="chat-box__messages__videos" controls>
                    <track kind="captions" />
                    <source src={data.content} type="video/mp4" />
                  </video>
                )}

          </p>
          <TimeAgo datetime={data.createdAt} locale={t('chatContent.language')} className="chat-box__messages__time" />
        </div>
      ))
        : (
          <div className="chat-box__messages__logo">
            <img
              src={Logo}
              alt="logo"
              width="100"
              height="100"
            />
          </div>
        )
            }
      </div>
    </C.ChatRightContent>

  );
}

export default MessageComponent;

MessageComponent.propTypes = {
  foundMessages: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.object])).isRequired,
  userId: PropTypes.string.isRequired,
};
