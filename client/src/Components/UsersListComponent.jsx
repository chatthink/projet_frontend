import React, { useEffect, useState } from 'react';
import { Container } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
import SpinnerComponent from './SpinnerComponent';
import UserCardComponent from './UserCardComponent';
import * as S from '../Styles/Components/jsx/card';
import * as I from '../Styles/Components/jsx/input';
import { getData } from '../Actions/UserActions';

function UsersListChatComponent() {
  // Get the dispatch function from the Redux store
  const dispatch = useDispatch();

  // Get the 't' function from the 'global' translation namespace
  const [t] = useTranslation('global');

  // Initialize the 'isLoading' state variable and the 'setIsLoading' function to update it
  const [isLoading, setIsLoading] = useState(false);

  // Initialize the 'users' state variable and the 'setUsers' function to update it
  const [users, setUsers] = useState([]);

  // Get the 'userId' value from the Redux store
  const userId = useSelector((state) => state.userReducer.user);

  // Initialize the 'filteredResults' state variable and
  // the 'setFilteredResults' function to update it
  const [filteredResults, setFilteredResults] = useState([]);

  // Initialize the 'searchInput' state variable and the 'setSearchInput' function to update it
  const [searchInput, setSearchInput] = useState('');

  // Run an effect whenever the 'userId' or 'dispatch' values change
  useEffect(() => {
    // Async function to fetch a list of users from the API
    async function getUserList() {
      // Make a request to the API to get a list of users
      const response = await dispatch(getData('users', 'get_users'));
      // Set the 'isLoading' state variable to true
      setIsLoading(true);
      // Update the 'users' state variable with the list of users from the API response
      setUsers(response.payload.data.users);
      // Set the 'isLoading' state variable to false
      setIsLoading(false);
    }
    // Call the 'getUserList' function
    getUserList();
  }, [userId, dispatch]);

  // Function to filter the list of users based on the search input value
  const searchItems = (searchValue) => {
    // Update the 'searchInput' state variable with the search value
    setSearchInput(searchValue);
    // If the 'searchInput' value is not an empty string
    if (searchInput !== '') {
      // Filter the 'users' array based on the search input value
      const filteredData = users.filter((item) => Object.values(item)
        .join('')
        .toLowerCase()
        .includes(searchInput.toLowerCase()));
      // Update the 'filteredResults' state variable with the filtered data
      setFilteredResults(filteredData);
    } else {
      // If the 'searchInput' value is an empty string,
      // set the 'filteredResults' state variable to the original 'users' array
      setFilteredResults(users);
    }
  };

  const usersListToMap = searchInput.length > 1 ? filteredResults : users;
  const usersList = usersListToMap.map((item) => (item._id !== userId ? <UserCardComponent key={item._id} item={item} /> : ''));
  return (
    <Container>
      <div className="chat-box__users-list__card">
        <I.ContainerInput>
          <I.Input
            placeholder={t('userContent.search_user')}
            onChange={(e) => searchItems(e.target.value)}
          />
        </I.ContainerInput>
        <S.Card>
          {isLoading ? <SpinnerComponent /> : usersList}
        </S.Card>
      </div>
    </Container>
  );
}

export default UsersListChatComponent;
