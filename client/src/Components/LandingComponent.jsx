import React from 'react';
import { useTranslation } from 'react-i18next';

function LandingComponent() {
  // Use the 'useTranslation' hook from the 'react-i18next'
  // library to get the 't' function for translations
  const [t] = useTranslation('global');

  return (
    <div className="landing">
      <div className="landing__first-section">
        <div>
          <div className="landing__first-section__left-section">
            <p className="landing__first-section__left-section--subtitle">
              {t('landingContent.what_is_chathink')}
            </p>
            <p className="landing__first-section__left-section--title col-md-7">
              {t('landingContent.share')}
            </p>
            <p className="landing__first-section__left-section--description col-md-7">
              {t('landingContent.description')}
            </p>
          </div>
          <img src="./assets/images/background--image.png" alt="Landing Background" />
        </div>
      </div>
    </div>
  );
}

export default LandingComponent;
