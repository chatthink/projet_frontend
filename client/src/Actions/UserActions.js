import axios from 'axios';
import { API_URL } from '../Utils/config';

export const postData = (dataToSubmit, routeName, typeName, hasCredentials) => async (dispatch) => {
  // Initialize the responseData object
  let responseData = {};
  // Check if the request has credentials
  if (hasCredentials) {
    // Send a POST request with credentials
    responseData = await axios.post(`${API_URL}/${routeName}`, dataToSubmit, {
      withCredentials: true,
    });
  } else {
    // Send a POST request without credentials
    responseData = await axios.post(`${API_URL}/${routeName}`, dataToSubmit);
  }
  // Dispatch the action with the response data as the payload
  return dispatch({
    type: typeName,
    payload: responseData,
  });
};

export const getData = (routeName, typeName) => async (dispatch) => {
  // Send a GET request with credentials
  const responseData = await axios.get(`${API_URL}/${routeName}`, {
    withCredentials: true,
  });
  // Dispatch the action with the response data as the payload
  return dispatch({
    type: typeName,
    payload: responseData,
  });
};
