const setMessageIsRead = (data) => ({
  type: 'set_messageis_read',
  payload: data,
});

export default setMessageIsRead;
