export default function chatReducer(state = {}, action = {}) {
  // Check the action type
  switch (action.type) {
    // If the action type is 'set_messageis_read', update the state
    case 'set_messageis_read':
      return { ...state, msgRead: action.payload };
    // If the action type is not recognized, return the current state
    default:
      return state;
  }
}
