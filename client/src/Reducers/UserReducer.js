export default function userReducer(state = {}, action = {}) {
  switch (action.type) {
    case 'register_user':
      return { ...state, register: action.payload };
    case 'login_user':
      return {
        ...state,
        token: action.payload.data.token,
        userId: action.payload.data.user._id,
      };
    case 'auth_user':
      return {
        ...state,
        user: action.payload.data.user._id,
        username: action.payload.data.user.username,
      };
    case 'logout_user':
      return { ...state };
    case 'get_users':
      return { ...state, users: action.payload };
    case 'get_all_messages':
      return { ...state, messages: action.payload };
    default:
      return state;
  }
}
