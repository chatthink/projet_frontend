import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { getData } from '../Actions/UserActions';

export default function VerifyUser(Component) {
  // Define the AuthenticationCheck component
  function AuthenticationCheck({ history, location, match }) {
    // Define the prop types
    AuthenticationCheck.propTypes = {
      history: PropTypes.oneOfType([PropTypes.object]).isRequired,
      location: PropTypes.oneOfType([PropTypes.object]).isRequired,
      match: PropTypes.oneOfType([PropTypes.object]).isRequired,
    };

    // Extract the user ID from the state
    const stateUserID = (state) => state.userReducer.user;
    const userID = useSelector(stateUserID);
    // Get the dispatch function
    const dispatch = useDispatch();
    // Fetch the data when the component mounts
    useEffect(() => {
      const fetchData = async () => {
        try {
          // Send a request to authenticate the user
          await dispatch(getData('auth', 'auth_user'));
        } catch (error) {
          // console.log(error.message);
        }
      };
      fetchData();
    }, [dispatch, history, userID]);

    // Render the Component with the user ID as a prop
    return <Component history={history} location={location} match={match} user={userID} />;
  }
  // Return the AuthenticationCheck component
  return AuthenticationCheck;
}
