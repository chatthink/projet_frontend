import React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import LandingView from './Views/LandingView';
import './Styles/scss/app.scss';
import VerifyUser from './HOC/VerifyUser';
import LoginView from './Views/LoginView';
import RegisterView from './Views/RegisterView';
import UsersView from './Views/UsersView';
import ChatView from './Views/ChatView';
import ChatBoxComponent from './Components/ChatBoxComponent';
import NavbarLayout from './Layouts/NavbarLayout';

// This is the root component for the application
function App() {
  // This component returns a JSX element with the following structure:
  return (
  // A `Router` component from the `react-router-dom` library is used to handle client-side routing
    <Router>
      <NavbarLayout />
      <Route exact path="/" component={LandingView} />
      <Route exact path="/login" component={(LoginView)} />
      <Route exact path="/register" component={(RegisterView)} />
      <Route exact path="/users" component={VerifyUser(UsersView)} />
      <Route exact path="/chats" component={VerifyUser(ChatView)} />
      <Route exact path="/chats/:id" component={VerifyUser(ChatBoxComponent)} />
    </Router>

  );
}

export default App;
