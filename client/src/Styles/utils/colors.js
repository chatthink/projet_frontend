export default function getColor() {
  const accentColors = [
    '#595cff',
    '#ee0979',
    '#F3904F',
    '#A770EF',
    '#00c3ff',
    '#64f38c',
  ];
  const colorsRandom = accentColors[Math.floor(Math.random() * accentColors.length)];

  return colorsRandom;
}

export const colors = {
  primary: '#140e26',
  secondary: getColor(),
  white: '#fff',
  purple: '#140441',
};
