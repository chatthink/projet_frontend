import styled from 'styled-components';
import { colors } from '../../utils/colors';

export const Nav = styled.nav`
display: flex;
height: 10vh;
justify-content: space-between;
transition: all 600ms ease;
box-shadow: 0 0 0.7rem ${colors.secondary};
background-color: ${colors.purple};
width: 100%;
top: 0;
z-index: 70;
position: fixed;

`;

export const NavSection = styled.div`
display: flex;
gap: 24px;
margin-top: 26px;
`;

export const NavItem = styled.a`
text-decoration: none;
text-align: center;
transition: 300ms ease-in-out;
color: ${colors.white} !important;
&:hover {
color: ${colors.secondary} !important;
}
`;
