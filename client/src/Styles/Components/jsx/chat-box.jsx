import styled from 'styled-components';
import { colors } from '../../utils/colors';

export const ChatContent = styled.div`
height: 70vh;
border-radius: 5px;
width: 120vh;
display: flex;
flex-direction: row;
background-color: ${colors.purple};
box-shadow: 0 0 0.1rem ${colors.white}, 0 0 0.1rem ${colors.white}, 0 0 1rem ${colors.secondary},
0 0 0.5rem ${colors.secondary}, 0 0 0.1rem ${colors.secondary},
inset 0 0 0.7rem ${colors.secondary};
@media only screen and (max-width: 800px) {
  width: 95%;
  display: unset;
  flex-direction: unset;
  box-shadow: none;
}
`;

export const ChatRightSection = styled.div`
position: relative;
border-left:none;
height: inherit;
width: inherit;
border-radius: 0 5px 5px 0;
border-left: 0px;
display: flex;
flex-direction: column;
@media screen and (max-width: 800px) {
  width: unset;
  box-shadow: none;

}
`;

export const ChatRightContent = styled.div`
border-bottom: 2px solid ${colors.secondary};
height: 100%;
overflow-y: auto;
`;

export const ChatDiv = styled.div`
border-top: 2px solid ${colors.secondary};
`;
export const ChatLeftSection = styled.div`
height: 100%;
width: 60vh;
border-right: 2px solid ${colors.secondary};
border-radius: 5px 0 0 5px;
display: flex;
flex-direction: column;
justify-content: space-between;
@media screen and (max-width: 800px) {
  width: 100%;
  border-right: unset;
}
`;
