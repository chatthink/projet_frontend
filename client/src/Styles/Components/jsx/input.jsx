import styled from 'styled-components';
import { colors } from '../../utils/colors';

export const Input = styled.input`
background: transparent;
border: none;
opacity: 0.9;
padding: 0px 4px;
font-size: 1.1em;
transition: 600ms ease;
border-bottom: solid white 2px;
color: white;

&:focus {
  transition: 600ms ease;
  border-bottom: solid ${colors.secondary} 2px;
  opacity: 1;
  outline: none;
}

&:focus {
  &::placeholder {
    transition: 600ms ease;
  }
}
`;

export const InputMessage = styled.input`
background: transparent;
box-shadow: 0 0 0.7rem ${colors.secondary};
border-radius: 5px;
&:focus {
  transition: 600ms ease;
box-shadow: 0 0 0.1rem ${colors.white}, 0 0 0.1rem ${colors.white}, 0 0 1rem ${colors.secondary},
0 0 0.5rem ${colors.secondary}, 0 0 0.1rem ${colors.secondary},
inset 0 0 0.7rem ${colors.secondary};
}
`;
export const ContainerInput = styled.div`
justify-content: center;
display: flex; 
margin-top: 20px;
margin-bottom: 20px;
`;
