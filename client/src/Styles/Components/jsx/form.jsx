import styled from 'styled-components';
import { colors } from '../../utils/colors';

export const ContainerForm = styled.div`
box-shadow: 0 0 0.1rem ${colors.white}, 0 0 0.1rem ${colors.white}, 0 0 1rem ${colors.secondary},
0 0 0.5rem ${colors.secondary}, 0 0 0.1rem ${colors.secondary},
inset 0 0 0.7rem ${colors.secondary};
height: 65vh;
width: 35vw;
display: flex;
justify-content: center;
text-align: center;
border-radius: 5px;
margin: 18vh auto;
@media only screen and (max-width: 700px) {
  width: 65vw;
}

@media only screen and (max-width: 700px) {
  box-shadow: none;
} 

@media only screen and (max-width: 1110px) {
  width: 55vw;

}

`;

export const Nav = styled.nav`
display: flex;
height: 10vh;
justify-content: space-between;
transition: all 600ms ease;
box-shadow: 0 0 5px ${colors.secondary};
background-color: ${colors.purple};
`;
