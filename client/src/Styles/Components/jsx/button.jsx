import styled from 'styled-components';
import { colors } from '../../utils/colors';

export const TextButton = styled.p`
`;
export const Button = styled.button`
margin-top: 15px;
cursor: pointer;
background: transparent;
color: ${colors.secondary};
border-radius: 32px;
border: solid ${colors.secondary} 2px;
position: relative;
overflow: auto;
height: 70px
&::after {
content: '';
position: absolute;
background-color: ${colors.secondary};
border-radius: 0px;
z-index: -1;
display: block;
transition: all 0.25s;
}
&:hover {
color: ${colors.white};
background-color: ${colors.secondary};

}
`;
