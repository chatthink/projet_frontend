import styled from 'styled-components';
import { colors } from '../../utils/colors';

export const Card = styled.div`
display: grid;
grid-template-columns: repeat(auto-fit, minmax(320px, 1fr));
gap: 20px;
padding: 15px;
`;

export const CardItem = styled.div`
transition: all 0.3s ease-in-out;
margin-top: 20px;
margin-bottom: 20px;
width: 360px;
display: block;
transform: scale(0.9);
&:hover {
box-shadow: 0 0 0.1rem ${colors.white}, 0 0 0.1rem ${colors.white}, 0 0 1rem ${colors.secondary},
0 0 0.5rem ${colors.secondary}, 0 0 0.1rem ${colors.secondary},
inset 0 0 0.7rem ${colors.secondary};
}
`;

export const CardDate = styled.h4`
color: ${colors.secondary} !important;
`;
