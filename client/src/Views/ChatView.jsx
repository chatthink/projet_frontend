import React from 'react';
import ChatBoxComponent from '../Components/ChatBoxComponent';

function ChatView(props) {
  return (
    <ChatBoxComponent props={props} />
  );
}

export default ChatView;
