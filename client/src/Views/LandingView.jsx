import React from 'react';
import { Container } from 'react-bootstrap';
import LandingComponent from '../Components/LandingComponent';

function LandingView() {
  return (
    <Container>
      <LandingComponent />
    </Container>
  );
}

export default LandingView;
