import React from 'react';
import UsersListComponent from '../Components/UsersListComponent';

function UsersView() {
  return (
    <UsersListComponent />
  );
}

export default UsersView;
