import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { postData } from '../Actions/UserActions';
import ImageAuth from '../Assets/images/image--right.png';
import * as F from '../Styles/Components/jsx/form';
import * as I from '../Styles/Components/jsx/input';
import * as B from '../Styles/Components/jsx/button';

// This function is a login view component that accepts a `history` object as a prop
function LoginView({ history }) {
  // This line destructures the `t` value from the
  // `useTranslation` hook with a namespace of 'global'
  const [t] = useTranslation('global');

  //  // This line gets the `dispatch` function from the `useDispatch`
  // hook, which is used to dispatch actions to the redux store
  const dispatch = useDispatch();

  // This line initializes an `error` state variable with an initial value
  // of `undefined`, and a `setError` function to update the error state variable
  const [error, setError] = useState();

  // This line initializes a `state` variable with an initial
  // value of an object containing an empty email and password field,
  // and a `setState` function to update the state variable
  const [state, setState] = useState({
    email: '',
    password: '',
  });

  // This function is called when the value of an input field changes.
  // It updates the corresponding field in the `state` object with the new value.
  const handleChange = (e) => {
    // Destructure the `id` and `value` properties from the event target
    const { id, value } = e.target;
    // Update the corresponding field in the `state` object
    // with the new value, and preserve the previous state values
    setState((prevState) => ({
      ...prevState,
      [id]: value,
    }));
  };

  // This is an async function that is called when the login form is submitted
  const loginSubmitHandler = async (e) => {
    // Prevent the default form submission behavior
    e.preventDefault();
    try {
      // Dispatch a redux action with the `postData` action creator,
      // passing in the `state` object, a string 'login',
      // and a string 'login_user' as arguments.
      // This will make an HTTP request to the server to log in the user.
      const response = await dispatch(
        postData(state, 'login', 'login_user', true),
      );
      // If the request is successful, navigate to the '/chats' route
      if (response) {
        history.push('/chats');
      }
    } catch (err) {
      // If there is an error, set the error state variable
      // to the error message returned by the server
      setError(err.response.data.message);
      // console.log(err.response.data.message);
    }
  };
  return (
    <div className="auth">
      {error && (
      <div className="auth__error">
        <p>{error}</p>
      </div>
      )}
      <img src={ImageAuth} alt="Shape Auth" />
      <F.ContainerForm>
        <form onSubmit={loginSubmitHandler}>
          <p>{t('authContent.welcome_login')}</p>
          <p>{t('authContent.connect_you')}</p>
          <div className="mt-5">
            <I.ContainerInput>
              <I.Input
                id="email"
                defaultValue={state.email}
                onChange={handleChange}
                placeholder={t('cardContent.mail')}
                type="email"
                required
              />
            </I.ContainerInput>
            <I.ContainerInput className="mt-5">
              <I.Input
                id="password"
                defaultValue={state.password}
                onChange={handleChange}
                placeholder={t('authContent.password')}
                type="password"
                required
              />
            </I.ContainerInput>
          </div>
          <B.Button type="submit">{t('authContent.connexion')}</B.Button>
          <div className="auth__span">
            <span>{t('authContent.not_account')}</span>
            <div>
              <Link to="/register">{t('authContent.register')}</Link>
            </div>
          </div>
        </form>
      </F.ContainerForm>
    </div>
  );
}

export default LoginView;

LoginView.propTypes = {
  history: PropTypes.oneOfType([PropTypes.object]).isRequired,
};
