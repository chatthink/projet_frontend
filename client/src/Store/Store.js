import { combineReducers } from 'redux';
import userReducer from '../Reducers/UserReducer';
import chatReducer from '../Reducers/ChatReducer';

const rootReducer = combineReducers({
  userReducer,
  chatReducer,
});
export default rootReducer;
