# ChatThink Frontend

<p align="center"><img src="https://app.mintsoulz.click/static/media/logo.1d23fff6.png" width="100"></p>

## Introduction

This app allows you to send real-time messages to friends and family. It uses the MERN stack (**MongoDB, Express, React, Node.js**) and **Socket.io** for real-time communication.

This application allows multiple users to chat in real time using Socket.io and ReactJS.

## Prerequisite

Before you can use the application, you will need the following: 

- **Node.js**
- **ReactJS**
- **Socket.io**

## Installation

To install the application on your computer, please follow these steps:

1. Clone this repository on your computer using `git clone https://gitlab.com/chatthink/projet_frontend.git`

2. Access the application directory using `cd projet_frontend/client`

3. Install the dependencies using `npm install`

4. Start the application using `npm start`

The application should now be accessible by opening your browser at `httpp://localhost:3000`.

## URL Deployment

[ChatThink](https://app.mintsoulz.click/)

## URL Figma
[Figma ChatThink](https://www.figma.com/file/d3rzjCPVDfplCWsEw2wBAX/Untitled?node-id=101%3A85&t=HlWzQhwkQ7LFlqGX-1)

## UML Frontend

[URL Frontend](https://lucid.app/lucidchart/dacfbee0-f2c4-4ca3-86e8-d26b0657166b/edit?viewport_loc=-187%2C-11%2C2219%2C1075%2C0_0&invitationId=inv_daad02d6-ed9c-43b4-b532-ad30b14359c7)

## Norme Commit 

**[ADD] -- if you add files, features, and so on**

**[FIX] -- if you were working on a bug or any form of default that you corrected**

**[DEL] -- if you removed files, features, assets, and so on**

**[UP] -- if you change something without adding any features or content**
